package com.personal.customer.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: CustomerController <br/>
 * <b>Copyright</b>: 2022 Pacifico Seguros - La Chakra <br/>.
 *
 * @author 2022  Pacifico Seguros - La Chakra <br/>
 * <u>Service Provider</u>: Teamsoft <br/>
 * <u>Developed by</u>: Teamsoft <br/>
 * <u>Changes:</u><br/>
 * <ul>
 *   <li>
 *     Junio 26, 2022 Creación de Clase.
 *   </li>
 * </ul>
 */
@RestController
public class CustomerController {

  @GetMapping("/hello")
  public Mono<String> helloWorld() {
    return Mono.just("Hello world");
  }


}
